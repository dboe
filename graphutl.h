extern GdkPixmap * ReadDib(char * name);

extern WORD GetDibWidth(BYTE *);

extern void rect_draw_some_item(HBITMAP src,RECT src_rect,HBITMAP dest,RECT dest_rect, short trans, short main_win);
extern void fry_dc(HWND hwnd,HDC dc);

void DisposeGWorld(HBITMAP bitmap);
void SectRect(RECT *a, RECT *b, RECT *c) ;
Boolean Button();
HBITMAP load_pict(short pict_num,HDC model_hdc);
void paint_pattern(HBITMAP dest,short which_mode,RECT dest_rect,short which_pattern);

