#include "worldarea.h"
#include <gtk/gtk.h>
#include <gdk/gdkevents.h>

G_DEFINE_TYPE(WorldArea, world_area, GTK_TYPE_DRAWING_AREA);

GtkWidget *world_area_new() {
	return g_object_new(WORLD_AREA_TYPE, NULL);
}

static gboolean world_area_expose(GtkWidget *w, GdkEventExpose *e) {
	return FALSE;
}

gboolean load_map(gchar *fname) {

	return TRUE;
}

static void world_area_class_init(WorldAreaClass *c) {
	GtkWidgetClass *wc;
	wc = GTK_WIDGET_CLASS(c);
	wc->expose_event = world_area_expose;
}

static void world_area_init(WorldArea *w) {
}
