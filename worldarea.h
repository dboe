#ifndef WORLD_AREA_H_DEFINED
#define WORLD_AREA_H_DEFINED

#include <gtk/gtk.h>

typedef struct _WorldArea {
	GtkDrawingArea parent;
} WorldArea;

typedef struct _WorldAreaClass {
	GtkDrawingAreaClass parent_class;
} WorldAreaClass;

#define WORLD_AREA_TYPE							(world_area_get_type())
#define WORLD_AREA(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), WORLD_AREA_TYPE, WorldArea))
#define WORLD_AREA_CLASS(obj)       (G_TYPE_CHECK_CLASS_CAST ((obj), WORLD_AREA, WorldAreaClass))
#define IS_WORLD_AREA(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WORLD_AREA_TYPE))
#define IS_WORLD_AREA_CLASS(obj)    (G_TYPE_CHECK_CLASS_TYPE ((obj), WORLD_AREA_TYPE))
#define WORLD_AREA_GET_CLASS				(G_TYPE_INSTANCE_GET_CLASS ((obj), WORLD_AREA_TYPE, WorldAreaClass))

GtkWidget *world_area_new();
gboolean load_map(gchar *fname);

#endif
