#ifndef TYPES_H_DEFINED
#define TYPES_H_DEFINED

#include <gtk/gtk.h>

typedef char Boolean;

typedef struct rect {
	int left,top,right,bottom;
} RECT;

typedef void* HWND;
typedef void* HDC;
typedef void* HBITMAP;
typedef void* HFILE;

typedef struct point {
	int x,y;
} POINT;

typedef unsigned int UINT;
typedef long int LONG;
typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long DWORD;

//#define FALSE 1
//#define TRUE 0

#define GFSR_GDIRESOURCES 0
#define GFSR_USERRESOURCES 1
#define HMENU void*
#define MF_BYCOMMAND 4
#define MF_ENABLED 5
#define MF_GRAYED 6
#define MK_CONTROL 7
#define VK_DOWN 8
#define VK_ESCAPE 8
#define VK_LEFT 10
#define VK_NUMPAD0 11
#define VK_NUMPAD1 12
#define VK_NUMPAD2 13
#define VK_NUMPAD3 14
#define VK_NUMPAD4 15
#define VK_NUMPAD5 16
#define VK_NUMPAD6 17
#define VK_NUMPAD7 18
#define VK_NUMPAD8 19
#define VK_NUMPAD9 20
#define VK_RIGHT 21
#define VK_UP 22
#define WM_CHAR 23
#define WM_COMMAND 24
#define WM_KEYDOWN 25
#define WM_LBUTTONDOWN 26
#define WM_PAINT 27
#define WM_RBUTTONDOWN 28
#define SB_CTL 29

/*
//actions.cc
DrawMenuBar
EnableMenuItem
GetFreeSpace
GetFreeSystemResources
GetMenu
GetScrollPos
InvertRect
PtInRect
SetFocus
SetViewportOrg
#define LOWORD 0
#define MAKEPOINT 1
*/

void OffsetRect(RECT *r, int dx, int dy) {
	r->right += dx;
	r->left += dx;
	r->top += dy;
	r->bottom += dy;
}

Boolean PtInRect(const RECT *r, const POINT p) {
	return (p.x >= r->left && p.x <= r->right &&
		p.y >= r->bottom && p.y <= r->top);
}

void SetFocus(HWND hwnd) {
}

int GetScrollPos(HWND hwnd, int flags) {
	return 0;
}

#define MAKEPOINT(wrd) (POINT p; p.x = wrd&0xFFFF; p.y = wrd >> 8;)

HDC GetDC(HWND hwnd) {
	return NULL;
}

#endif
