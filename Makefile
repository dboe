CC=gcc
CFLAGS=-std=c99 `pkg-config --cflags gtk+-2.0`
LDFLAGS=`pkg-config --libs gtk+-2.0`
EXE=dboe
#OBJS=actions.o blades.o combat.o dlgutils.o dlogtool.o exlsound.o fields.o fileio.o global.o graphics.o graphutl.o gutils.o infodlgs.o itemdata.o items.o locutils.o monster.o newgraph.o party.o specials.o startup.o text.o town.o townspec.o
OBJS=main.o worldarea.o global.o fileio.o

#.cc:
#	$(CXX) $(CFLAGS) $@

all: dboe

dboe: $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o $(EXE)

.PHONY: clean
clean:
	rm -f *.o $(EXE)
