#define xmin(a,b)	((a) < (b) ?  (a) : (b))

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include "global.h"
#include "text.h"
#include "string.h"
#include "exlsound.h"

#include "graphutl.h"

extern short ulx,uly;
//extern HBITMAP bw_bitmap,mixed_gworld,dialog_pattern_gworld,pattern_gworld,status_pattern_gworld;
extern char file_path_name[256];

Boolean pal_ok = FALSE;
Boolean syscolors_stored = FALSE;
//	int elements[5] = {COLOR_ACTIVEBORDER,COLOR_ACTIVECAPTION,
//			COLOR_WINDOWFRAME,COLOR_SCROLLBAR,COLOR_BTNFACE};
//	COLORREF store_element_colors[5];
short dlog_pat_placed = 0;
short current_pattern = -1;

GdkPixmap * ReadDib(char * name) {
	char real_name[256] = "", *name_ptr;
	short i, last_slash = -1;

	for (i = 0; i < 256; i++)
		if ((file_path_name[i] == 92) || (file_path_name[i] == '/'))
			last_slash = i;
		if (last_slash < 0)
			strcpy((char *) real_name,name);
		else {
			strcpy(real_name,file_path_name);
			name_ptr = (char *) real_name;
			name_ptr += last_slash + 1;
			sprintf((char *) name_ptr,"%s",name);
			//real_name -= last_slash + 1;
			//ASB(real_name);
		}

	GError *err;
//	gdk_pixbuf_new_from_file_at_scale
	GdkPixbuf *pb = gdk_pixbuf_new_from_file(real_name, &err);
	return gdk_pixmap_create_from_xpm(GDK_DRAWABLE(main_win), NULL, NULL, real_name);
}

// FIX transparency
void rect_draw_some_item(GdkPixmap * src, RECT src_rect, GdkPixmap * dest, RECT dest_rect, short trans, short main_win) {
//	HBITMAP transbmp;
	GdkColor white, black, x, oldcolor;
//	COLORREF white = RGB(255,255,255),black = RGB(0,0,0),oldcolor,x = RGB(17,17,17);
	RECT debug = {0,0,200,20};
	UINT c;
//	HBITMAP store,store2;
	Boolean dlog_draw = FALSE;
	gdk_color_parse("#FFFFFF", &white);
	gdk_color_parse("#000000", &black);
	gdk_color_parse("#A7A7A7", &x);

	cairo_t *ctx = gdk_cairo_create(game);
	gdk_cairo_set_source_pixmap(ctx, src, src_rect.left, src_rect.top);
	cairo_paint(ctx);
	cairo_destroy(ctx);
//	gdk_cairo_set_source_pixbuf(ctx, src, src_rect.left, src_rect.top);
//	StretchBlt(hdcMem2, dest_rect.left, dest_rect.top, dest_rect.right - dest_rect.left,
//									dest_rect.bottom - dest_rect.top,
//									hdcMem,src_rect.left,src_rect.top,src_rect.right - src_rect.left,
//									src_rect.bottom - src_rect.top);
//		}
	}
}

void DisposeGWorld(HBITMAP bitmap) {
	DeleteObject(bitmap);
}

void SectRect(RECT *a, RECT *b, RECT *c) {
	IntersectRect(c,a,b);
}

// which_mode is 0 ... dest is a bitmap
// is 1 ... ignore dest ... paint on mainPtr
// is 2 ... dest is a dialog, use the dialog pattern
// both pattern gworlds are 192 x 256
void paint_pattern(HBITMAP dest,short which_mode,RECT dest_rect,short which_pattern)
{
	HBITMAP source_pat;
	RECT pattern_source = {32,168,96,232}, pat_dest_orig = {0,0,64,64},pat_dest;
	short i,j;

	RECT draw_from_orig = {0,0,192,256},draw_from,draw_to;
	short store_ulx,store_uly;

	if (which_mode == 2) {
		source_pat = dialog_pattern_gworld;
		if (dlog_pat_placed == 0) {
			dlog_pat_placed = 1;
			OffsetRect(&pattern_source, 64 * 2,0);
			for (i = 0; i < 3; i++)
				for (j = 0; j < 4; j++) {
					pat_dest = pat_dest_orig;
					OffsetRect(&pat_dest,64 * i, 64 * j);
					rect_draw_some_item(mixed_gworld,pattern_source,
						dialog_pattern_gworld,pat_dest,0,0);
					}
			}
		}
		else {
			source_pat = pattern_gworld;
			if (current_pattern != which_pattern) {
            current_pattern = which_pattern;
				OffsetRect(&pattern_source, 64 * (which_pattern % 5),
					64 * (which_pattern / 5));
				for (i = 0; i < 3; i++)
					for (j = 0; j < 4; j++) {
						pat_dest = pat_dest_orig;
						OffsetRect(&pat_dest,64 * i, 64 * j);
						rect_draw_some_item(mixed_gworld,pattern_source,
							pattern_gworld,pat_dest,0,0);
						}
				}
			}

	// now patterns are loaded, so have fun
	// first nullify ul shifting
	store_ulx = ulx;
	store_uly = uly;
	ulx = uly = 0;
	for (i = 0; i < (dest_rect.right / 192) + 1; i++)
		for (j = 0; j < (dest_rect.bottom / 256) + 1; j++) {
			draw_to = draw_from_orig;
			OffsetRect(&draw_to,192 * i, 256 * j);
			IntersectRect(&draw_to,&draw_to,&dest_rect);
			if (draw_to.right != 0) {
				draw_from = draw_to;
				OffsetRect(&draw_from, -192 * i, -256 * j);
				switch (which_mode) {
					case 0:
						rect_draw_some_item(source_pat,draw_from,
							dest,draw_to,0,0); break;
					case 1:
						rect_draw_some_item(source_pat,draw_from,
							source_pat,draw_to,0,1); break;
					case 2:
						rect_draw_some_item(source_pat,draw_from,
							dest,draw_to,0,2); break;
					}
				}
			}
	ulx = store_ulx;
	uly = store_uly;
}

HBITMAP load_pict(short pict_num,HDC model_hdc) {
	HBITMAP got_bitmap;

	switch(pict_num) {
		case 700: case 701: case 702: got_bitmap = ReadDib("blscened/STATAREA.BMP",model_hdc); break;
		case 703: got_bitmap = ReadDib("blscened/TEXTBAR.BMP",model_hdc); break;
		case 704: got_bitmap = ReadDib("blscened/BUTTONS.BMP",model_hdc); break;
		case 705: got_bitmap = ReadDib("blscened/TERSCRN.BMP",model_hdc); break;
		case 800: got_bitmap = ReadDib("blscened/TER1.BMP",model_hdc); break;
		case 801: got_bitmap = ReadDib("blscened/TER2.BMP",model_hdc); break;
		case 802: got_bitmap = ReadDib("blscened/TER3.BMP",model_hdc); break;
		case 803: got_bitmap = ReadDib("blscened/TER4.BMP",model_hdc); break;
		case 804: got_bitmap = ReadDib("blscened/TER5.BMP",model_hdc); break;
		case 805: got_bitmap = ReadDib("blscened/TER6.BMP",model_hdc); break;
		case 820: got_bitmap = ReadDib("blscened/TERANIM.BMP",model_hdc); break;
		case 821: got_bitmap = ReadDib("blscened/FIELDS.BMP",model_hdc); break;
		case 830: got_bitmap = ReadDib("blscened/STARTUP.BMP",model_hdc); break;
		case 831: got_bitmap = ReadDib("blscened/STANIM.BMP",model_hdc); break;
		case 832: got_bitmap = ReadDib("blscened/STARTBUT.BMP",model_hdc); break;
		case 850: got_bitmap = ReadDib("blscened/DLOGPICS.BMP",model_hdc); break;
		case 851: got_bitmap = ReadDib("blscened/SCENPICS.BMP",model_hdc); break;
		case 860: got_bitmap = ReadDib("blscened/TALKPORT.BMP",model_hdc); break;
		case 875: got_bitmap = ReadDib("blscened/DLOGMAPS.BMP",model_hdc); break;
		case 880: got_bitmap = ReadDib("blscened/MISSILES.BMP",model_hdc); break;
		case 900: got_bitmap = ReadDib("blscened/TINYOBJ.BMP",model_hdc); break;
		case 901: got_bitmap = ReadDib("blscened/OBJECTS.BMP",model_hdc); break;
		case 902: got_bitmap = ReadDib("blscened/PCS.BMP",model_hdc); break;
		case 905: got_bitmap = ReadDib("blscened/PCS.BMP",model_hdc); break;
		case 903: case 904: got_bitmap = ReadDib("blscened/MIXED.BMP",model_hdc); break;
//		case 903: case 904: got_bitmap = ReadDib("blscened/blscened\MIXED.BMP",model_hdc); break;
		case 910: case 911: case 912: got_bitmap = ReadDib("blscened/BIGSCEN.BMP",model_hdc); break;
		case 1100: case 1200: got_bitmap = ReadDib("blscened/MONST1.BMP",model_hdc); break;
		case 1101: case 1201: got_bitmap = ReadDib("blscened/MONST2.BMP",model_hdc); break;
		case 1102: case 1202: got_bitmap = ReadDib("blscened/MONST3.BMP",model_hdc); break;
		case 1103: case 1203: got_bitmap = ReadDib("blscened/MONST4.BMP",model_hdc); break;
		case 1104: case 1204: got_bitmap = ReadDib("blscened/MONST5.BMP",model_hdc); break;
		case 1105: case 1205: got_bitmap = ReadDib("blscened/MONST6.BMP",model_hdc); break;
		case 1106: case 1206: got_bitmap = ReadDib("blscened/MONST7.BMP",model_hdc); break;
		case 1107: case 1207: got_bitmap = ReadDib("blscened/MONST8.BMP",model_hdc); break;
		case 1108: case 1208: got_bitmap = ReadDib("blscened/MONST9.BMP",model_hdc); break;
		case 1109: case 1209: got_bitmap = ReadDib("blscened/MONST10.BMP",model_hdc); break;
		case 1400: got_bitmap = ReadDib("blscened/STSCICON.BMP",model_hdc); break;
		case 1401: got_bitmap = ReadDib("blscened/HELPPICS.BMP",model_hdc); break;
		case 1402: got_bitmap = ReadDib("blscened/APPIC.BMP",model_hdc); break;
		case 1500: case 1501: case 1502: case 1503: case 1504: case 1505: case 1506: case 1507:
			got_bitmap = ReadDib("blscened/BIGMAPS.BMP",model_hdc); break;
		case 2000: got_bitmap = ReadDib("blscened/DLOGBTNS.BMP",model_hdc); break;
		case 3000: got_bitmap = ReadDib("blscened/START.BMP",model_hdc); break;
		case 3001: got_bitmap = ReadDib("blscened/SPIDLOGO.BMP",model_hdc); break;

		default: got_bitmap = NULL;
		}
	return got_bitmap;
}
