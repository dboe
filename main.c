#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include "worldarea.h"

gboolean key_press_cb(GtkWidget *w, GdkEvent *e, gpointer d) {
	switch (e->key.keyval) {
		case GDK_q: gtk_main_quit();
								break;
		case GDK_w: printf("joooooooo\n");
								// game_win->move_up();
								break;
		default: return FALSE;
							break;
	}

	return TRUE;
}

int main(int argc, char **argv) {
	gtk_init(&argc, &argv);

	GtkWidget *main_win = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	gtk_window_set_title(GTK_WINDOW(main_win), "Deadly Blades of Exile");

	gtk_widget_add_events(main_win, GDK_KEY_PRESS_MASK);
	g_signal_connect(main_win, "key-press-event", G_CALLBACK(key_press_cb), NULL);
	g_signal_connect(main_win, "destroy", G_CALLBACK(gtk_main_quit), NULL);

	GtkWidget *game_win = world_area_new();
//	GtkDrawingArea *game_win = GTK_DRAWING_AREA(gtk_drawing_area_new());
	gtk_drawing_area_size(GTK_DRAWING_AREA(game_win), 400, 400);
	//g_signal_connect(G_OBJECT(game_win), "expose_event", G_CALLBACK(expose_cb), NULL);
	//set_keys(game_win, keys)
	//set_level()
	//set_position
	//set_party

	GtkToolbar *toolbar = GTK_TOOLBAR(gtk_toolbar_new());
	gtk_toolbar_set_orientation(toolbar, GTK_ORIENTATION_HORIZONTAL);
	gtk_toolbar_set_style(toolbar, GTK_TOOLBAR_TEXT);
	gtk_toolbar_set_tooltips(toolbar, FALSE);

	GtkToolItem *btn1 = gtk_tool_button_new(NULL, "one");
	GtkToolItem *btn2 = gtk_tool_button_new(NULL, "two");
	gtk_toolbar_insert(toolbar, btn1, -1);
	gtk_toolbar_insert(toolbar, btn2, -1);

	GtkListStore *list1 = gtk_list_store_new(1, G_TYPE_STRING);
	GtkTreeIter l_i;
	gtk_list_store_append(list1, &l_i);
	gtk_list_store_set(list1, &l_i, 0, "Tralala", -1);
	gtk_list_store_append(list1, &l_i);
	gtk_list_store_set(list1, &l_i, 0, "Mnum", -1);

	GtkWidget *tree1 = gtk_tree_view_new_with_model(GTK_TREE_MODEL(list1));
	GtkTreeViewColumn *col1 = gtk_tree_view_column_new();
	GtkCellRenderer *cell1 = gtk_cell_renderer_text_new();

	gtk_tree_view_column_set_title(col1, "Sloupec");
	gtk_tree_view_append_column(GTK_TREE_VIEW(tree1), col1);
	gtk_tree_view_column_pack_start(col1, cell1, TRUE);
	gtk_tree_view_column_add_attribute(col1, cell1, "text", 0);

	GtkWidget *v_gt = gtk_vbox_new(FALSE, 0);
	GtkWidget *v_inv = gtk_vbox_new(FALSE, 0);
	GtkWidget *h_all = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(v_gt), GTK_WIDGET(game_win), FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(v_gt), GTK_WIDGET(toolbar), FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(v_inv), GTK_WIDGET(tree1), FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(h_all), v_gt, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(h_all), v_inv, TRUE, TRUE, 0);
	gtk_container_add(GTK_CONTAINER(main_win), GTK_WIDGET(h_all));

	GdkColor color;
	gdk_color_parse("#FF0000", &color);
	gtk_widget_modify_bg(GTK_WIDGET(game_win), GTK_STATE_NORMAL, &color);
	gdk_color_parse("#00FF00", &color);
	gtk_widget_modify_bg(GTK_WIDGET(toolbar), GTK_STATE_NORMAL, &color);
	gdk_color_parse("#0000FF", &color);
	gtk_widget_modify_bg(GTK_WIDGET(tree1), GTK_STATE_NORMAL, &color);

	gtk_widget_show_all(main_win);

	gtk_main();

	return 0;
}


